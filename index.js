const asyncHandler = require("express-async-handler")
const { resolve, URL } = require("url")
const fetch = require("node-fetch")
const fixtures = require("./fixtures.js")

// TODO: move those const to global conf
const gitlabCatalogSlug = "dms-catalog"
const gitlabApiUrl = "https://framagit.org/api/v4/" // TODO add GITLAB_API to .env
const gitlabOrganization = "brunetton"

async function request(urlStr) {
  const url = new URL(urlStr)
  const response = await fetch(url)
  if (response.status !== 200) {
    throw response
  }
  return await response.json()
}

async function getPackageFromGitlab(organizationName, packageName) {
  // Get catalog
  let projectPath = `${organizationName}%2F${gitlabCatalogSlug}` // url-encoded project path
  const catalog = await request(
    resolve(
      gitlabApiUrl,
      `projects/${projectPath}/repository/files/datapackage.json/raw?ref=master`
    )
  )
  // Find dataset url from catalog
  const datasetInfos = catalog.resources.find(e => e.name == packageName)
  const datasetUrl = datasetInfos.path
  // Get datapackage content from datapackage.json file
  const response = await fetch(new URL(datasetUrl))
  if (response.status !== 200) {
    // console.log(response)
    throw response
  }
  return await response.json()
}

module.exports = function(app) {
  // Mocks
  // fixtures.initMocks();
  // Copied / pasted from `routes/dms.js` and modified a little
  app.get(
    "/:owner/:name",
    asyncHandler(async (req, res, next) => {
      const datapackage = await getPackageFromGitlab(
        req.params.owner,
        req.params.name
      )

      // Since "datapackage-views-js" library renders views according to
      // descriptor's "views" property, we need to generate view objects:
      datapackage.views = datapackage.views || []

      // Data Explorer used a slightly different spec
      datapackage.dataExplorers = []

      // Create a visualization per resource as needed
      datapackage.resources.forEach((resource, index) => {
        // Convert bytes into human-readable format:
        resource.size = resource.size
          ? bytes(resource.size, { decimalPlaces: 0 })
          : resource.size

        let controls = {
          showChartBuilder: false,
          showMapBuilder: false,
        }

        const view = {
          id: index,
          title: resource.title || resource.name,
          resources: [resource.name],
          specType: null,
        }

        if (resource.format) {
          resource.format = resource.format.toLowerCase()
        }

        // Add 'table' views for each tabular resource:
        const tabularFormats = ["csv", "tsv", "dsv", "xls", "xlsx"]
        let chartView, tabularMapView

        if (tabularFormats.includes(resource.format)) {
          // Default table view
          view.specType = "table"
          // DataExplorer specific view to render a chart from tabular data
          chartView = Object.assign({}, view)
          chartView.specType = "simple"
          // DataExplorer specific view to render a map from tabular data
          tabularMapView = Object.assign({}, view)
          tabularMapView.specType = "tabularmap"
        } else if (resource.format.includes("json")) {
          // Add 'map' views for each geo resource:
          view.specType = "map"
        } else if (resource.format === "pdf") {
          view.specType = "document"
        }

        // Determine when to show chart builder
        const chartBuilderFormats = ["csv", "tsv"]

        if (chartBuilderFormats.includes(resource.format))
          controls = { showChartBuilder: true, showMapBuilder: true }

        const views = tabularMapView
          ? [view, chartView, tabularMapView]
          : [view]
        const dataExplorer = JSON.stringify({
          resources: [resource],
          views,
          controls,
        }).replace(/'/g, "&#x27;")

        // Add Data Explorer item per resource
        datapackage.dataExplorers.push(dataExplorer)
        datapackage.views.push(view)
      })

      // const profile = await Model.getProfile(req.params.owner);
      res.render("showcase.html", {
        title: req.params.owner + " | " + req.params.name,
        dataset: datapackage,
        owner: {},
        // owner: {
        //   name: profile.name,
        //   title: profile.title,
        //   description: utils.processMarkdown.render(profile.description),
        //   avatar: profile.image_display_url || profile.image_url
        // },
        thisPageFullUrl:
          req.protocol + "://" + req.get("host") + req.originalUrl,
        dpId: JSON.stringify(datapackage).replace(/'/g, "&#x27;"), // keep for backwards compat?
      })
    })
  )

  app.get(
    "/search",
    asyncHandler(async (req, res, next) => {
      // const result = await Model.search(req.query);
      // Pagination
      const from = req.query.from || 0
      const size = req.query.size || 10
      const total = 4
      const totalPages = Math.ceil(total / size)
      const currentPage = parseInt(from, 10) / size + 1
      // const pages = utils.pagination(currentPage, totalPages);
      const pages = 1

      datapackage_json = await request(
        `https://framagit.org/api/v4/projects/${gitlabOrganization}%2Fdms-catalog/repository/files/datapackage.json/raw?ref=master`
      )

      // Add some fields to prevent package_list_show macro from crash
      const resources = await Promise.all(
        datapackage_json.resources.map(async dataset => {
          return {
            name: dataset.name, // This should be done in a better way to avoid too much requests to Gitlab,
            title: dataset.title,
            organization: { name: gitlabOrganization }, // TODO
            metadata_modified: "TODO", // TODO
            // resources: "?" // TODO
          }
        })
      )
      const result = {
        results: resources,
        count: datapackage_json.length,
      }
      res.render("search.html", {
        title: "Search",
        result,
        query: req.query,
        totalPages,
        pages,
        currentPage,
      })
    })
  )
}
