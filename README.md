# GitLab CKAN-NG plugin

This is a plugin for [CKAN-NG](https://github.com/datopian/frontend-v2).

## Install

Install CKAN-NG if not already done:
```bash
git clone https://github.com/datopian/frontend-v2.git
cd frontend-v2
npm install
```

Install this plugin:
```bash
cd plugins
git clone https://framagit.org/jailbreak/opendata-platform/ckan-ng-plugin-gitlab.git gitlab
cd gitlab
npm install
cd ../..
```

Edit `.env` to add `gitlab` to `PLUGINS` env variable.

Run frontend-v2:

```bash
npm run dev
```

See also [CKAN-NG doc](https://github.com/datopian/frontend-v2#plugins)